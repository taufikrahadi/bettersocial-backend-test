import { IsNotEmpty, IsString } from 'class-validator';
import { IsExists } from 'nestjs-rest-api-validation-kit';
import { User } from 'src/application/user/entities/user.entity';

export class SignInDto {
  @IsExists('username', User, {
    message: ({ value }) => `User '${value}' not found`,
  })
  @IsNotEmpty()
  username: string;

  @IsString()
  @IsNotEmpty()
  password: string;
}
