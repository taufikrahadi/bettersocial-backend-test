import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { SignInDto } from './dto/sign-in.dto';
import { JwtService } from '@nestjs/jwt';
import { compareSync } from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  public async signIn(signInDto: SignInDto) {
    try {
      const user = await this.userService.findByUsername(signInDto.username);

      if (!this.comparePassword(signInDto.password, user.password))
        throw new UnprocessableEntityException(`Wrong Password`);

      const jwtPayload = {
        id: user.id,
        fullname: user.fullname,
        username: user.username,
      };
      const accessToken = this.jwtService.sign(jwtPayload);

      return { ...jwtPayload, accessToken };
    } catch (error) {
      throw error;
    }
  }

  private comparePassword(password: string, hashed: string): boolean {
    return compareSync(password, hashed);
  }
}
