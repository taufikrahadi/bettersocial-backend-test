import { genSaltSync, hashSync } from 'bcrypt';
import { Exclude } from 'class-transformer';
import { BaseEntityObject } from '../../../common/class/base.entity';
import { BeforeInsert, Column, Entity } from 'typeorm';

@Entity()
export class User extends BaseEntityObject {
  @Column({
    unique: true,
  })
  username: string;

  @Column()
  fullname: string;

  @Column({
    select: false,
  })
  @Exclude()
  password: string;

  @Column({
    nullable: true,
    transformer: {
      from(value) {
        return process.env.APP_URL + '/' + value;
      },
      to(value) {
        return value;
      },
    },
  })
  profilePic?: string;

  @BeforeInsert()
  setPassword?() {
    this.password = hashSync(this.password, genSaltSync(12));
  }
}
