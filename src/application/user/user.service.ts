import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { SignUpDto } from './dto/sign-up.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  public async signUp(signUpDto: SignUpDto) {
    try {
      const { fullname, username, password } = signUpDto;
      const user = await this.create({
        fullname,
        username,
        password,
      });

      return user;
    } catch (error) {
      throw error;
    }
  }

  public async create(payload: SignUpDto) {
    try {
      const user = await this.userRepository.save(
        this.userRepository.create(payload),
      );

      return user;
    } catch (error) {
      throw error;
    }
  }

  public async findById(id: string) {
    try {
      const user = await this.userRepository.findOneBy({ id });

      return user;
    } catch (error) {
      throw error;
    }
  }

  public async findByUsername(username: string) {
    try {
      const user = await this.userRepository.findOne({
        where: {
          username,
        },
        select: ['id', 'username', 'password', 'fullname'],
      });

      if (!user) throw new UnprocessableEntityException(`User not found`);

      return user;
    } catch (error) {
      throw error;
    }
  }

  public async updateProfilePic(id: string, filename: string) {
    try {
      const user = await this.userRepository.update(id, {
        profilePic: filename,
      });

      return user;
    } catch (error) {
      throw error;
    }
  }
}
