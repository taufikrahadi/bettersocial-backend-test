import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpStatus,
  ParseFilePipeBuilder,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { UserService } from './user.service';
import { SignUpDto } from './dto/sign-up.dto';
import { ValidateUsernameDto } from './dto/validate-username.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { join } from 'path';
import * as nanoid from 'nanoid';
import { JwtAuthGuard } from '../../common/guards/auth.guard';
import { Userinfo } from '../../common/decorators/userinfo.decorator';

@Controller('v1/user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('/sign-up')
  @UseInterceptors(ClassSerializerInterceptor)
  async signUp(@Body() signUpDto: SignUpDto) {
    return this.userService.signUp(signUpDto);
  }

  @Post('/validate-username')
  async validateUsername(@Body() validateUsernameDto: ValidateUsernameDto) {
    return validateUsernameDto;
  }

  @Post('/profile-pic')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(
    FileInterceptor('profilePic', {
      storage: diskStorage({
        destination: join(process.cwd(), 'public'),
        filename: (req: any, file: Express.Multer.File, cb) => {
          const random = nanoid.nanoid(8);
          cb(null, random + '.' + file.mimetype.split('/')[1]);
        },
      }),
    }),
  )
  async changeProfilePic(
    @Userinfo('id') userId: string,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: 'jpeg',
        })
        .build({
          errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
        }),
    )
    file: Express.Multer.File,
  ) {
    return this.userService.updateProfilePic(userId, file.filename);
  }

  @Get('/profile')
  @UseGuards(JwtAuthGuard)
  async myProfile(@Userinfo('id') userId: string) {
    return this.userService.findById(userId);
  }
}
