import {
  IsAlphanumeric,
  IsNotEmpty,
  IsString,
  MinLength,
} from 'class-validator';
import { IsUnique } from 'nestjs-rest-api-validation-kit';
import { User } from '../entities/user.entity';

export class SignUpDto {
  @IsAlphanumeric()
  @IsNotEmpty()
  @IsUnique('username', User, {
    message: ({ value }) => `Username '${value}' has already been taken`,
  })
  username: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  password: string;

  @IsNotEmpty()
  @IsString()
  fullname: string;
}
