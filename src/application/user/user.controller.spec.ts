import { Test } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { SignUpDto } from './dto/sign-up.dto';

describe('UserController', () => {
  let userController: UserController;

  beforeAll(async () => {
    const moduleTest = await Test.createTestingModule({
      controllers: [UserController],
    })
      .useMocker((token) => {
        const results = {
          id: '1b3208a6-6c58-4771-ad55-ab3325a2ad45',
          fullname: 'Testing User',
          password: 'P4assword',
          username: 'testinguser',
        };

        if (token === UserService)
          return { signUp: (data: any) => ({ ...data, id: results.id }) };
      })
      .compile();

    userController = moduleTest.get(UserController);
  });

  describe('sign-up', () => {
    it('should return registered user', async () => {
      const payload: SignUpDto = {
        fullname: 'Testing User',
        password: 'P4assword',
        username: 'testinguser',
      };

      const result = await userController.signUp(payload);

      expect(result).toMatchObject({
        id: expect.any(String),
        fullname: payload.fullname,
        password: payload.password,
        username: payload.username,
      });
    });
  });
});
