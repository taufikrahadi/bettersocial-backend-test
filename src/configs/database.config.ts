import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (env: ConfigService) => {
        return {
          type: 'postgres',
          host: env.get<string>('DB_HOST'),
          port: env.get<number>('DB_PORT'),
          username: env.get<string>('DB_USERNAME'),
          password: env.get<string>('DB_PASSWORD'),
          database: env.get<string>('DB_NAME'),
          synchronize: true,
          autoLoadEntities: true,
        };
      },
    }),
  ],
})
export class DatabaseConfig {}
