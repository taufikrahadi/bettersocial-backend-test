import { NestFactory } from '@nestjs/core';
import { AppModule } from './application/app.module';
import { UnprocessableEntityException, ValidationPipe } from '@nestjs/common';
import { ValidationError } from 'class-validator';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors: ValidationError[]) => {
        const messages = errors.map((error) => {
          const { property, constraints } = error;

          const keys = Object.keys(constraints);

          const msgs: string[] = [];

          keys.forEach((key) => {
            msgs.push(`${constraints[key]}`);
          });

          return {
            property,
            errors: msgs,
          };
        });

        throw new UnprocessableEntityException(messages);
      },
    }),
  );

  app.setGlobalPrefix('api');
  await app.listen(process.env.APP_PORT);
}
bootstrap();
